package com.example.app2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.fragment.app.Fragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toPlus : Button = FindViewById(R.id.toPlus)
        val toMinus = FindViewById<Button>(R.id.toMinus)


        toPlus.setOnClickListener{

            val plusFragment = PlusFragment()

            val fragment : Fragment? =

            supportFragmentManager.findFragmentByTag(PlusFragment::class.java.simpleName)

            if(fragment !is PlusFragment) {
                supportFragmentManager.beginTransaction()
                    .add(R.id.Linear, plusFragment, PlusFragment::class.java.simpleName)
                    .commit()
            }

        }
    }
}